<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User ;
use App\Models\Blog;
use App\Http\Requests\Admin\addBlog;
use App\Http\Requests\Admin\editBlog;

use Illuminate\Support\Facades\Auth;
class BlogController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function GetAddBlog()
    {   
        $id = Auth::user()->id;
        $getData = User::findOrFail($id);
        return view('admin.blog.add',compact('getData'));
    }
    
    public function PostAddBlog(addBlog $request)
    {
        $data = $request->all();
       
        if($request->hasFile('images')){
            $file = $request->images;
            // Tên files (Name)
            $file_name = $file->getClientOriginalName();
            // đuôi file (Extension)
            $file_tail = $file->getClientOriginalExtension();
            // đường dẫn tạm thời
            $file_temp = $file->getRealPath();
            // kích cỡ file
            $file_syze = $file->getSize();
            // Kiểu file 
            $file_type = $file->getMimeType();
            // =----------UPLOAD----------
            $file->move('upload\blog', $file_name);
            
            $data['images'] = $file_name;
            // create array
            $data = [
                'title' => $data['title'],
                'author' => $data['author'],
                'description' => $data['description'],
                'images' => $data['images']
            ];

        }

        $result = Blog::create($data);
        if($result) {
            return redirect('admin/blog/list')->with('success',"Insert success");
        }else {
            echo "fail";
        }
    }

    public function GetListBlog(){
        $id = Auth::user()->id;
        $getData = User::findOrFail($id);
        $getList = Blog::paginate(5); // Phân trang : get all data in database ,mmỗi trang lấy 5 item 
        return view('admin.blog.list',compact('getData','getList'));
    }

    public function GetEditBlog($id){
        $idData = Auth::user()->id;
        $getData = User::findOrFail($idData);
        $getEdit = Blog::findOrFail($id);
        return view('admin.blog.edit',compact('getData','getEdit'));
    }

    public function PostEditBlog($id , editBlog $request ) {
        $data = $request->all();
        if($request->hasFile('images')) {
            $file = $request->images ;
                        // Tên files (Name)
            $file_name = $file->getClientOriginalName();
            // đuôi file (Extension)
            $file_tail = $file->getClientOriginalExtension();
            // đường dẫn tạm thời
            $file_temp = $file->getRealPath();
            // kích cỡ file
            $file_syze = $file->getSize();
            // Kiểu file 
            $file_type = $file->getMimeType();
            // =----------UPLOAD----------
            $file->move('upload\blog', $file_name);
            
            $data['images'] = $file_name;
            // create array
            $data = [
                'title' => $data['title'],
                'author' => $data['author'],
                'description' => $data['description'],
                'images' => $data['images']
            ];
        }else {
             $getEdit = Blog::findOrFail($id);
             $data = [
                'title' => $data['title'],
                'author' => $data['author'],
                'description' => $data['description'],
                'images' => $getEdit['images'] 
            ];
        }
        Blog::where('id',$id)->update($data);
        return redirect()->back()->with('success','Update Success');
    }

    public function PostDelBlog($id){
        $result = Blog::where('id',$id)->delete();

        if($result) {
            return redirect()->back()->with('success','Xoa Thanh Cong');
        }else {
            return redirect()->back()->with('fail','Xoa that bai');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
