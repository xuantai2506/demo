<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Country;
use App\Http\Requests\Admin\countries;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function GetCountries()
    {   
        $id = Auth::user()->id;
        $getData = User::findOrFail($id);
        $getCountry = Country::all();
        return view('admin.countries',compact('getData','getCountry'));
    }
    public function PostCountries(countries $request){
        $country = $request->countries;
        $data = [
            'name_country'=>$country
        ];

        $result = Country::create($data);

        if($result){
            return redirect()->back()->with('success','Thêm thành phố thành công');
        }else {
            return redirect()->back()->withErrors('fail','Thêm thất bại');
        }

    }
    public function PostDelCountries($id){
        $result = DB::table('countries')->where('id',$id)->delete();

        if($result) {
            return redirect()->back()->with('success','Xoa Thanh Cong');
        }else {
            return redirect()->back()->with('fail','Xoa that bai');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
