<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\profile;
use App\Models\User;
use App\Models\Country;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    // function GET
    public function index()
    {
        $id = Auth::user()->id;
        $getData = User::findOrFail($id);
        return view('admin.index',compact('getData'));
    }
    public function GetFormBasic()
    {
        $id = Auth::user()->id;
        $getData = User::findOrFail($id);
        return view('admin.form-basic',compact('getData'));
    }

    public function GetPagesProfile()
    {   
        $id = Auth::user()->id;
        $getData = User::findOrFail($id);
        $getCountry = Country::all();
        return view('admin.pages-profile',compact('getData','getCountry'));
    }

    // function POST
    public function PostPagesProfile(profile $request){
        $id = Auth::user()->id;
        $data = request()->except(['_token']);
        $pass = $request->password;

        $password = Auth::User()->password;           
        // xử lý password
        if(!empty($pass)) {
            $data['password'] = bcrypt($pass);
        }else {
            $data['password'] = $password;
        }

        // xử lý file images 
        if($request->hasFile('images')){
            $file = $request->images;
            // Tên files (Name)
            $file_name = $file->getClientOriginalName();
            // đuôi file (Extension)
            $file_tail = $file->getClientOriginalExtension();
            // đường dẫn tạm thời
            $file_temp = $file->getRealPath();
            // kích cỡ file
            $file_syze = $file->getSize();
            // Kiểu file 
            $file_type = $file->getMimeType();
            // =----------UPLOAD----------
            $file->move('upload\user\avatar', $file_name);
            $data['images'] = $file_name ;
        }else {
            $request->images = Auth::User()->images;
        }

        User::where('id', $id)->update($data);
        return redirect()->back();
        // return redirect()->action('UserController@GetPagesProfile');
    }
}
