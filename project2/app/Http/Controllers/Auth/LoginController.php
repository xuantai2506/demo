<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    // public function login($request){
    //     $email = $request->email;
    //     $password = $request->password ;

    //     $data = [
    //         'email' => $email,
    //         'password' => $password,
    //         'level' => 0
    //     ];
    //     if(Auth::attempt($data, false)){
    //         return redirect('/admin/home');
    //     }else {
    //         return redirect('/admin/login');
    //     }
    // }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //  public function guard()
    // {
    //     return Auth::guard('admin');
    // }
   
    public function logout(Request $request) {
      Auth::logout();
      return redirect('/admin/login');
    }
}
