<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Frontend\account;
use App\Http\Requests\Frontend\requestAddProduct;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\User ;
use App\Models\Country ;
use App\Models\comment ;
use App\Models\reply ;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idAccount = Auth::user()->id;
        $account = User::findOrFail($idAccount);
        $country = Country::all();
        return view('frontend.account.account',compact('account','country'));
    }
    //Get List Product
    public function GetListProduct() {
        $listProduct = Product::all();
        return view('frontend.account.listProduct',compact('listProduct'));
    }
    //Get Add Product
    public function GetNewProduct(){
        $category = Category::all();
        $brand = Brand::all();
        return view('frontend.account.addProduct',compact('category','brand'));
    }
    // show infor user
    public function PostAccount(account $request){
        $idAccount = Auth::user()->id;
        if($request->hasFile('images')){
            $file = $request->images;
            // Tên files (Name)
            $file_name = $file->getClientOriginalName();
            // đuôi file (Extension)
            $file_tail = $file->getClientOriginalExtension();
            // đường dẫn tạm thời
            $file_temp = $file->getRealPath();
            // kích cỡ file
            $file_syze = $file->getSize();
            // Kiểu file 
            $file_type = $file->getMimeType();
            // =----------UPLOAD----------
            // $file->move('upload\user\avatar', $file_name);
            $file->move('upload\member', $file_name);
            $request->images = $file_name;
        }else {
            $request->images = Auth::user()->images  ;
        }
        // check password
        if(isset($request->password)){
            $password = bcrypt($request->password);
        }else {
            $password = Auth::user()->password;
        }

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,
            'phone_number' => $request->phone_number,
            'messages' => $request->messages ,
            'id_country' => $request->id_country,
            'images' => $request->images,
            'level' => 1
        ];
        $result = User::where('id',$idAccount)->update($data);
        if($result){
            comment::where('member',Auth::user()->name)->update(['images'=>$request->images]); //thay doi image ben comment để phù hợp với blog/single
            reply::where('name',Auth::user()->name)->update(['images'=>$request->images]); //thay đổi image bên reply để phù hợp với blog/single
            return redirect()->back()->with('success',"Update Success");
        }
    }
    // Upload images vào folder và database  
    public function uploadImages($fileImages){
        $arrImages = [];
        foreach ($fileImages as $images) {
                $name = strtotime(date('Y-m-d H:i:s'))."_".$images->getClientOriginalName(); 
                $small = "small_".$name;
                $large = "large_".$name;

                $path = public_path('upload/product/'. $name);
                $path2 = public_path('upload/product/'. $small);
                $path3 = public_path('upload/product/'. $large);

                Image::make($images->getRealPath())->save($path);
                Image::make($images->getRealPath())->resize(50, 70)->save($path2);
                Image::make($images->getRealPath())->resize(200, 300)->save($path3);

                array_push($arrImages,$name);
            }
            $pathImages = json_encode($arrImages);
            $data['images'] = $pathImages;
            return $pathImages;
    }
    
    public function PostNewProduct(requestAddProduct $request){
        $data = $request->all();
        $idUser = Auth::user()->id;
        $data['id_member'] = Auth::user()->id;
        $data['name_member'] = Auth::user()->name;
        if($request->hasFile('images')){
            $file = $request->file('images');
            $upload = AccountController::uploadImages($file);
            $data['images'] = $upload ;
        }
        if($data['status'] == 0){
            $data['percent'] = 0; 
        }else {
            if($data['percent'] == ""){
                $data['percent'] = 0 ;
            }else {
                $data['percent'] = $request->percent ;
            }
        }
        $result = Product::create($data);
        if($result){
            return redirect()->back()->with('success','Insert Success');
        }
    } 

    public function GetEditProduct($id){
        $category = Category::all();
        $brand = Brand::all();
        $listEdit = Product::findOrFail($id);
        return view('frontend.account.edit',compact('category','brand','listEdit'));
    }
    // xóa hình ảnh database và xóa trong folder
    public function removeImage($imgRemove , $imagesDB){
        foreach($imgRemove as $imgRemove){
                if(in_array($imgRemove, $imagesDB)){

                    $name_remove = $imgRemove ;
                    $small_remove = "small_".$name_remove;
                    $large_remove = "large_".$name_remove;

                    $key = array_search($imgRemove , $imagesDB);
                    unset($imagesDB[$key]); //xoa phan tu trong database

                    //xoa trong folder
                    unlink("upload/product/".$name_remove);
                    unlink("upload/product/".$small_remove);
                    unlink("upload/product/".$large_remove);
                }
            }
        $imagesDB = array_values($imagesDB);
        return $imagesDB ;
    }

    public function PostEditProduct(Request $request,$id){
        $data = [
            'name_product' => $request->name_product,
            'price' => $request->price,
            'category' => $request->category,
            'brand' => $request->brand ,
            'status' => $request->status,
            'percent' => $request->percent ,
            'profile' => $request->profile,
            'detail' => $request->detail,
        ];
        // mảng xóa -> khi được tick thì push vào 1 mảng -> gọi là mảng xóa 
        $imagesRemove = $request->checkbox;
        // mảng images có trong database -> có dạng string nên phải xử dụng json decode để convert thành arr
        $productImages = Product::where('id',$id)->get(['images']);
        //json_decode -> chuyển string thành mảng thường => imagesDB là mảng trong database hiện hành
        $imagesDB = json_decode($productImages[0]['images'],true);
        // Xoa  phan tu (khi click isset checkbox)
        if(isset($_POST['checkbox'])){
            $imagesDB = AccountController::removeImage($imagesRemove,$imagesDB);//mangr con lai
        }
        // upload file images
        if($request->hasFile('images')){
            $file = $request->file('images');
            $upload = AccountController::uploadImages($file);
            $imagesEdit = json_decode($upload); //mảng các hình vừa click upload
        }else {
            $imagesEdit = [];//mảng các hình vừa click upload
        }
        // nối 2 mảng lại với nhau (tổng mảng trong database tạm thời);
        $imagesUpload = array_merge($imagesDB , $imagesEdit);
        // dd($imagesUpload);
        if(count($imagesUpload) > 3){
            AccountController::removeImage($imagesEdit,$imagesUpload);
            return redirect()->back()->with('success','SẢN PHẨM NHIỀU HƠN 3, VUI LÒNG XÓA BỚT !!');
        }else {
            // chuyen mang thanh string thì mới up vào database được.
            $data['images'] = json_encode($imagesUpload);
            $result = Product::where('id',$id)->update($data);
            if($result){
                return redirect()->back()->with('success',"Update Success");        
            }
        }
    }
    // Hầm xóa sản phẩm 
    public function DeleteProduct($id){
        $result = Product::where('id',$id)->delete();
        if($result){
            return redirect()->back()->with('success','Delete success');
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
