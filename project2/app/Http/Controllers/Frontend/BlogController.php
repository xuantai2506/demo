<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Blog ;
use App\Models\comment ;
use App\Models\reply ;
use App\Models\rating ;
class BlogController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }
    public function GetListBlog(){
        $listBlog = Blog::paginate(2);
        return view('frontend.blog.list',compact('listBlog'));
    }

    public function GetSingleBlog($id) {
        $arr_rating = [];

        $singleBlog = Blog::findOrFail($id);

        $previous = Blog::where('id','<',$singleBlog->id)->max('id');//Chỉnh đi lùi lại trang blog

        $next = Blog::where('id','>' ,$singleBlog->id)->min('id');//Chỉnh tới trang blog 

        $listComment = comment::where('id_blog',$id)->get()->toArray(); //comment
        
        $listReplyComment = reply::get()->toArray(); // ---------------> reply comment

        // tính trung bình cộng rating
        $count_rating = rating::where('rating','>',0)->where('blog_id',$id)->pluck('rating');
        foreach($count_rating as $value){
            array_push($arr_rating, $value);
        }
        $sum = array_sum($arr_rating);
        $average = $sum / count($arr_rating); // -> compact laravel truyền average vào phần view blog.single.{{id_blog}}
        // kết thúc tính trung bình cộng
        return view('frontend.blog.single',compact('singleBlog','previous','next','listComment','listReplyComment','id','average'));

    }

    public function PostRating() {
        $id = Auth::id();
        $id_blog = $_POST['id_blog'];
        $rating = $_POST['values'];
        $arr_rating = [];
        // update rating 
        $result = rating::where("user_id",$id)->where("blog_id",$id_blog)->update(['rating'=>$rating]);

        // tính trung bình cộng rating
        $count_rating = rating::where('rating','>',0)->where('blog_id',$id_blog)->pluck('rating');
        foreach($count_rating as $value){
            array_push($arr_rating, $value);
        }
        
        $sum = array_sum($arr_rating);
        $average = $sum / count($arr_rating);
        // kết thúc tính trung bình cộng
        
        $data = [
            'user_id' => $id,
            'blog_id' => $id_blog ,
            'rating' => $rating
        ];

        if($result) {
            return view('frontend.blog.rating',compact('rating','average'));
        }else {
            rating::create($data);
            return $rating;
        }
        return view('frontend.blog.rating') ;
    }
    // Test Chia ham 
    public function Average(){
        $count_rating = rating::where('rating','>',0)->where('blog_id',$id_blog)->pluck('rating');
        foreach($count_rating as $value){
            array_push($arr_rating, $value);
        }
        
        $sum = array_sum($arr_rating);
        $average = $sum / count($arr_rating);
        return $average ;
    }
    // Test Chia ham 
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
