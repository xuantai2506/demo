<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Frontend\register ;
use Illuminate\Support\Facades\Auth;
use App\Models\User ;
use App\Models\Country ;
use App\Models\Checkout;
use App\Mail\DemoEmail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Support\Facades\Mail;
/* Include the Composer generated autoload.php file. */
require 'E:\XAMPP\XAMPP\composer\vendor\autoload.php';

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $product = CartController::getSession();
        return view('frontend.cart.cart',compact('product'));
    }

    public function getCheckout(){
        $getCountry = Country::all();
        $product = CartController::getSession();
        return view('frontend.cart.checkout',compact('product','getCountry'));
    }

    public function getSession(){
        if(\session()->has('product')){
            $arr = \session()->get('product');
            return $arr ;
        }else {
            $arr = [];
            return $arr ;
        }
    }
    // chưa login thì vào hàm này 
    public function RegisterUser(register $request){
        if($request->hasFile('images')){
            $file = $request->images;
            // Tên files (Name)
            $file_name = $file->getClientOriginalName();
            // đuôi file (Extension)
            $file_tail = $file->getClientOriginalExtension();
            // đường dẫn tạm thời
            $file_temp = $file->getRealPath();
            // kích cỡ file
            $file_syze = $file->getSize();
            // Kiểu file 
            $file_type = $file->getMimeType();
            // =----------UPLOAD----------
            $file->move('upload\member', $file_name);
            
            $request['images'] = $file_name;
            // create array
            $result = User::create([
                'name' => $request['name'],
                'password' => bcrypt($request['password']),
                'phone_number' => $request['phone_number'],
                'messages' => $request['messages'],
                'email' => $request['email'],
                'id_country' => $request['id_country'],
                'images' => $file_name,
                'level' => 1
            ]);
            
            if($result){
                $arr_id = User::where("email",$request->email)->pluck('id')->toArray();
                $id_user = $arr_id[0];
                CartController::checkout($request,$id_user,$request->email,$request->name);
            }
        }
    }
    // Đã login rồi thì vào hàm này 
    public function LoginCheckout(Request $request){
        $id_user = Auth::user()->id;
        $email = Auth::user()->email;
        $name = Auth::user()->name ;
        CartController::checkout($request,$id_user,$email,$name);
    }
    // Khách hàng thanh toán
    public function checkout($request,$id_user,$email,$name){ // -> cái này tượng trưng history
        $arrSession = CartController::getSession();
        $subtotal = 0;
        $arr_name_product = [];
        $quantity_product = 0 ;
        $check = true;
        
        // Tinhs tổng của giỏ hàng để đưa vào database
        foreach($arrSession as $key => $value){
            array_push($arr_name_product, $value['name_product']); // lấy tên sản phẩm vào 1 mảng
            //tính tổng tiền khách hàng đã mua được
            $subtotal += ($arrSession[$key]['quantity'] * $arrSession[$key]['price']);
            // tinhs tổng số lượng khách hàng mua sản phẩm
            $quantity_product = $quantity_product + $arrSession[$key]['quantity'];
        }
        // get id database 
        $get_id_user = User::where("email",$request->email)->pluck('id')[0];
        // list toan bộ danh sách khách mua hàng trong talbe Checkout (history)
        $checkout = Checkout::get()->toArray();
        foreach($checkout as $key => $value){
            // Nếu email tồn tại trong checkout(history) r ,thì thay thế quantity_product , list Product và subtotal .
            if($value['id_user'] == $get_id_user){
                CartController::updateCheckout($request,$email,$name,$quantity_product,$arr_name_product,$subtotal);
                $check = false ;
            }
        }
        // Nếu chưa tồn tại thì tạo mới trong checkout(history)
        if($check){
            echo "qua check = true r ne";
            CartController::createCheckout($request,$email,$name,$arr_name_product,$id_user,$quantity_product,$subtotal);
        }
    }
    //nếu khách hàng đã từng đặt hàng ,hệ thống sẽ update thêm số lượng product để tính khuyến mãi
    public function updateCheckout($request,$email,$name,$quantity_product,$arr_name_product,$subtotal){
        echo "qua update r ne";
        $id_user = Auth::user()->id;

        //quantity_update =   quantity_database + quantity_product (vừa được user mua)
        $quantity_product_update = Checkout::where('id_user',$id_user)->pluck('quantity_product')[0] + $quantity_product;
        $list_product = json_encode($arr_name_product);
        //select -> list_product_database dạng string ->convert sang 
        $list_product_database = json_decode(Checkout::where('id_user',$id_user)->pluck('list_product')[0]);
        //arr_name_product là list các sản phẩm khách hàng vừa mua 
        $list_product_update = array_merge($list_product_database,$arr_name_product);
        // lọc tên sản phẩm bị trùng trong list
        $list_product_update = array_unique($list_product_update);
        //convert list_product sang String ;
        $list_product_update = json_encode($list_product_update);
        //$subtotal_update = $subtotal_database + $subtotal
        $subtotal_database = Checkout::where('id_user',$id_user)->pluck('subtotal')[0];
        $subtotal_update = $subtotal_database + $subtotal ;
        // 
        $data_update = [
            'content_order' => $request->content_order,
            'quantity_product' => $quantity_product_update ,
            'list_product' => $list_product_update,
            'subtotal' => $subtotal_update
        ];
        $result = Checkout::where('id_user',$id_user)->update($data_update);
        if($result){
            CartController::sendEmail();
        }
        dd("Update : Mua Hàng thành công ");
    }
    // Nếu khách hàng chưa mua hàng bao giờ ,sẽ create đơn hàng trong checkout(history)
    public function createCheckout($request,$email,$name,$arr_name_product,$id_user,$quantity_product,$subtotal){
        //conver -> string
        $list_product = json_encode($arr_name_product);
        $data = [
            'id_user' => $id_user,
            'content_order' => $request->content_order,
            'quantity_product' => $quantity_product,
            'list_product' => $list_product,
            'subtotal' => $subtotal
        ];
        $result = Checkout::create($data);
        if($result){
            CartController::sendEmail();
            dd("Create: Mua Hàng Thành Công");
        }else {
            dd("not oke");
        }
    }
    public function sendEmail(){
        $arrSession = CartController::getSession();
        Mail::to('ducmaixuand7@gmail.com')->send(new DemoEmail($arrSession));
        echo "oke";
    }
    public function UpdateQuantity(Request $request){
        $id_product = $request->id_product;
        $result_input = $request->result_input;
        $arr = CartController::getSession();
        if($request->session()->has('product')){
            foreach($arr as $key => $value){
                if($arr[$key]['id'] == $id_product){
                    $arr[$key]['quantity'] = $result_input ;
                    //xoa phan tu trong mang
                    if($arr[$key]['quantity'] == 0){
                        unset($arr[$key]);
                    }
                    //end
                }
            }
            $request->session()->put('product',$arr);
        }
        return $result_input;
    }

    public function DeleteProduct(Request $request){
        $id_product = $request->id_product;
        $arr = CartController::getSession();
        if($request->session()->has('product')){
            foreach($arr as $key => $value){
                if($arr[$key]['id'] == $id_product){
                    unset($arr[$key]);
                }
            }
            array_values($arr);
            $request->session()->put('product',$arr);
        }
        return $id_product;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
