<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Frontend\requestComment;
use Illuminate\Support\Facades\Auth;
use App\Models\comment ;
use App\Models\reply ;
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function PostComment(requestComment $request , $idBlog){
        $result = Auth::user()->toArray();

        $data = [
            'id_blog' => $idBlog,
            'member' => $result['name'],
            'comment' => $request->comment ,
            'images' => $result['images']
        ];

        $comment = comment::create($data);

        if($comment) {
            return redirect('blog/'.$idBlog);
        }else {
            return redirect('blog/'.$idBlog);
        }
    }
    
    public function PostReplyComment(Request $request , $id) {
        $result = Auth::user()->toArray();
        $data = [
            'comment_id' => $id ,
            'name' => $result['name'],
            'reply' => $request->reply,
            'images' => $result['images']
        ];

        $reply = reply::create($data);
        if($reply) {
            return redirect()->back();
        }else {
            dd("fail");
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
