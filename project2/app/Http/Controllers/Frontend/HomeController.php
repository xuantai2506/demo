<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
class HomeController extends Controller
{   

      public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // trang chủ
    public function index()
    {
        $newProduct = []; // tạo lấy ra 6 object mới nhất trong mảng
        $ProductDatabase = array_reverse(Product::get()->toArray()); // list toàn bộ sản phẩm trong database
        for($i= 0 ; $i < count($ProductDatabase); $i++){
            array_push($newProduct,$ProductDatabase[$i]);
        }
        return view('frontend.index.index',compact('newProduct'));
    }
    // get toan bo san pham de phan trang trong trang product
    public function getProduct(){
        $arrPrice = [];
        $product = Product::paginate(9);
        // vòng lặp này để push cái giá trị price trong tổng product tìm được ,đê tìm min max -> xử lý phần data-slider
        foreach ($product as $key => $value) {
            array_push($arrPrice, $product[$key]['price']);
        }
        $valueMin = min($arrPrice); // value min
        $valueMax = max($arrPrice); //value max
        //nếu ti
        if($product){
            $arrSlider = [$valueMin , $valueMax + 1];
        }else {
            $arrSlider = [0,100001];
        }
        $arrSlider = json_encode($arrSlider);
        return view('frontend.index.product',compact('product','arrSlider'));
    }
    //get 1 san pham detail khi ta click vao
    public function getProductDetail(Request $request ,$idProduct){
        $arr = HomeController::getSession();
        $quantity = 0 ;
        if(!empty($arr)){
            foreach($arr as $key => $value){
                if($arr[$key]['id'] == $idProduct){
                    if($arr[$key]['quantity'] > 0){
                        $quantity = $arr[$key]['quantity'];
                    }
                }
            }
        }
        $detail_product = Product::findOrFail($idProduct);
        $brand = Brand::get()->toArray();
        return view('frontend.index.detail',compact('detail_product','brand','quantity'));
    }
    // set session
    public function setSession($request,$product){
        $check = true ;
        if($request->session()->has('product')){
            $arr = $request->session()->get('product');
            foreach($arr as $key => $value){
                if($value['id'] == $product['id']){
                    $arr[$key]['quantity'] = $arr[$key]['quantity'] + 1;
                    $request->session('product')->put('product',$arr);
                    $check = false ;
                }
            }
            if($check){
                $arr = $request->session()->get('product');
                array_push($arr,$product);
                array_values($arr);
                $request->session('product')->put('product',$arr);
            }
        }else {
            $arr = [];
            array_push($arr, $product);
            array_values($arr);
            $request->session()->put('product',$arr);
        }
    }
    // get session
    public function getSession(){
        if(\session()->has('product')){
            $arr = \session()->get('product');
            return $arr ;
        }
    }
    // add product 
    public function postAddProduct(Request $request){
        $id_product = $request->id_product;
        $getProduct = Product::findOrFail($id_product);
        $images = json_decode($getProduct['images']);
        $product_key = ['id','name_product','price','status','percent','quantity','images'];
        $product_value = [$getProduct['id'],$getProduct['name_product'],$getProduct['price'],$getProduct['status'],$getProduct['percent'],1,$images[0]];
        $product = array_combine($product_key,$product_value);
        HomeController::setSession($request , $product);
        return redirect()->back();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
