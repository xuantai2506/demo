<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Frontend\register ;
use App\Http\Requests\Frontend\login ;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User ;
use App\Models\Country ;
class MemberController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function GetRegister() {
        $getCountry = Country::all();
        return view('frontend.member.register',compact('getCountry'));
    }
    
    public function GetLogin(){
        return view('frontend.member.login');
    }

    public function PostLogin(login $request){
        $email = $request->email;
        $password = $request->password ;

        $data = [
            'email' => $email,
            'password' => $password,
            'level' => 1
        ];
        if(Auth::attempt($data, false)){
            return redirect('/blog/list');
        }else {
            return redirect('/member/login');
        }
    }

    public function PostRegister(register $request){
      if($request->hasFile('images')){
            $file = $request->images;
            // Tên files (Name)
            $file_name = $file->getClientOriginalName();
            // đuôi file (Extension)
            $file_tail = $file->getClientOriginalExtension();
            // đường dẫn tạm thời
            $file_temp = $file->getRealPath();
            // kích cỡ file
            $file_syze = $file->getSize();
            // Kiểu file 
            $file_type = $file->getMimeType();
            // =----------UPLOAD----------
            $file->move('upload\member', $file_name);
            
            $request['images'] = $file_name;
            // create array
            User::create([
                'name' => $request['name'],
                'password' => bcrypt($request['password']),
                'phone_number' => $request['phone_number'],
                'messages' => $request['messages'],
                'email' => $request['email'],
                'id_country' => $request['id_country'],
                'images' => $file_name,
                'level' => 1
            ]);
        }
      
      return redirect('member/login');
    }

    public function PostLogout() {
        Auth::logout();
        return redirect('/member/login');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
