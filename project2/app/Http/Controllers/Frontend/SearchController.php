<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Models\User ;
class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function arrSlider($product){
        $arrPrice = [];
        // vòng lặp này để push cái giá trị price trong tổng product tìm được ,đê tìm min max -> xử lý phần data-slider
        foreach ($product as $key => $value) {
            array_push($arrPrice, $product[$key]['price']);
        }
        //nếu ti
        if(count($arrPrice) >= 1){
            $valueMin = min($arrPrice); // value min
            $valueMax = max($arrPrice)+1; //value max
            $message = "Danh sách sản phẩm của bạn đã được tìm thấy ở phía dưới .";
        }else {
            $valueMin = 0 ;
            $valueMax = 1;
            $message = "Không tìm thấy sản phẩm của bạn !";
        }
        if($product){
            $arrSlider = [$valueMin , $valueMax + 1];
        }else {
            $arrSlider = [0,100001];
        }
        $arrSlider = json_encode($arrSlider);
        return $arrSlider;
    }

    //search theo o input :v 
    public function postSearch(Request $request){
        $search = $request->search;//lấy value trong ô input ở header (search)
        $newProduct = Product::where("name_product","like",'%'.$search.'%')->get(); // lọc phần tử có chữ cái $search
        
        $arrSlider = SearchController::arrSlider($newProduct);
        
        return view('frontend.search.search',compact('newProduct','arrSlider'));
    }
    
    // search theo gias tien (keo tha)
    public function SearchPrice(Request $request){
        $valueMin = $request->valueMin;
        $valueMax = $request->valueMax ;

        $newProduct = Product::whereBetween('price',[$valueMin,$valueMax])->get();
        return view('frontend.search.searchPrice',compact('newProduct'));
    }
    // get link sang trang search nanag cao
    public function searchAdvance(){
        $product = Product::get();
        $category = Category::get();
        $brand = Brand::get();
        $arrSlider = SearchController::arrSlider($product);
        return view('frontend.search.advance',compact('product','category','brand','arrSlider'));
    }
    //click search nanag cao
    public function postSearchAdvance(Request $request){
        $category = $request->category ;
        $brand = $request->brand;
        $status = $request->status;

        if($category > 0 && $brand > 0 && ($status ==0 || $status == 1)){
            $newProduct = Product::where('category',$category)->where('brand',$brand)->where('status',$status)->get();
        }else if($category ==0 && $brand != 0 && ($status ==0 || $status == 1)){ //lua chon status ,brand


            $newProduct = Product::where('status',$status)->where('brand',$brand)->get();
        }else if(($status != 1 && $status != 0) && $category != 0 && $brand != 0){ //lua chon brand,category
            $newProduct = Product::where('category',$category)->where('brand',$brand)->get();

        }else if($brand == 0 && $category != 0 && ($status ==0 || $status == 1)) {//lua chon category,status
            $newProduct = Product::where('category',$category)->where('status',$status)->get();
       
        }else if($category ==0 && $brand == 0){ //lua chon status
            $newProduct = Product::where('status',$status)->get();
        }else if($brand == 0 && $status != 1 && $status != 0) { //lua chon category
            $newProduct = Product::where('category',$category)->get();
        }else if($category == 0 && $status != 1 && $status != 0){ //lua chon brand
            $newProduct = Product::where('brand',$brand)->get();
        }else {
            $newProduct = Product::get();
        }
        return view('frontend.search.searchPrice',compact('newProduct'));
        // $newProduct = Product::get();
                           
        // if($request->category != 0) {
        //     $newProduct->where('category',$request->category);
        // }
        // if($request->brand != 0) {
        //     $newProduct->where('brand',$request->brand);
        // }
        // if($request->status != 2) {
        //     $newProduct->where('status',$request->status);
        // }
        
        // $newProduct->get();
    }
}
