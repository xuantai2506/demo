<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class editBlog extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5',
            'author'=>'required|min:5',
            'description'=> 'required|min:5',
        ];
    }
    public function messages(){
        return [
            'title.required' => "Please enter title",
            'author.required' => "Please enter author",
            'description.required' => "Please enter description",
            'min' => ':attribute not less than 5 ',
        ];
    }
}
