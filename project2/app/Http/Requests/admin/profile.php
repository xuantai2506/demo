<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class profile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'phone_number' => 'required|min:5',
            'messages' => 'required|min:5|max:255',
            'id_country' => 'required'
        ];
    }

    public function messages(){
        return [
            'name.required' => "Please enter name",
            'phone_number.required' => 'Please enter phone_number',
            'messages.required' => 'Please enter message',
            'id_country.required' => 'Please enter country',
            'min' => ':attribute not less than 5 ',
            'mãx' => ':attribute not greater than 255'
        ];
    }
}
