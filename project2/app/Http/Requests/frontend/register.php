<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'name' => 'required|min:5|max:255',
            'password' => 'required|min:5|max:255',
            'check_pass' => 'required|min:5|max:255',
            'email' => 'required|min:5|max:255',
             'phone_number' => 'required|min:5',
            'messages' => 'required|min:5|max:255',
            'id_country' => 'required',
            'images' => 'required'
        ];
    }
    public function messages(){
        return [
            'name.required' => "Please enter name",
            'pass.required' => "Please enter password",
            'check_pass.required' => "Please enter check_pass",
            'email.required' => "Please enter email",
            'images.required' => "Please enter file images",
            'phone_number.required' => 'Please enter phone_number',
            'messages.required' => 'Please enter message',
            'id_country.required' => 'Please enter country',
            'min' => ":attribute please don't min < 5",
            'max' => ":attribute please don't max > 255"
        ];
    }
}
