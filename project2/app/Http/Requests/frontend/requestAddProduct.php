<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class requestAddProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_product' => 'required | min:5',
            'price' => 'required',
            'category' => 'required',
            'brand' => 'required',
            'status' => 'required',
            'profile' => 'required',
            'images' => 'required',
            'detail' => 'required'
        ];
    }
    public function messages(){
        return [
            'name_product.required' => "Please enter name",
            'price.required' => "Please enter price",
            'category.required' => "Please enter category",
            'brand.required' => "Please enter brand",
            'status.required' => "Please enter status",
            'profile.required' => "Please enter profile",
            'images.required' => "Please enter images",
            'detail.required' => "Please enter detail",

            'min' => ":attribute please don't min < 5",
        ];
    }
}
