<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DemoEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $demo ;
    public function __construct($demo)
    {
        $this->demo = $demo ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('xuantai59@gmail.com')
                    ->view('frontend.email.email')
                    ->with(
                        [
                            "messages" => "Bạn đã mua hàng thành công",
                        ]
                    );
    }
}
