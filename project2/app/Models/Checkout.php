<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $table = "checkouts";
    protected $fillable = ['id_user','content_order','quantity_product','list_product','subtotal'];
}
