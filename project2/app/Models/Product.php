<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['id_member','name_member','name_product','category','price','brand','status','percent','profile','images','detail'];
}
