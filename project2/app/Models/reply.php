<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class reply extends Model
{
    protected $table = 'reply';
    protected $fillable = ['comment_id' ,'name','reply','images'];
}
