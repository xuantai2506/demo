<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone_number)');
            $table->string('messages');
            $table->string('country');
            $table->integer('level');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('users',function (Blueprint $table){
            $table->integer('id_country')->after('id');
            $table->foreign('id_country')->references('id_country')->on('tbl_country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
