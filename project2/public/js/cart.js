$(document).ready(function(){
	$('.cart_quantity_up').click(function(){
		$id_product = $(this).attr('id');
		$cart_quantity_input = $('#cart_input_'+$id_product).val();
		$price = $('#price_'+$id_product).val();
		$result_input = parseInt($cart_quantity_input) + 1;
		$subtotal = $('#subtotal').html();
		$total = $('#total_'+$id_product).html();
		$.ajax({
			type:"post",
			url :"update/quantity",
            headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			data : {
				id_product : $id_product,
				result_input : $result_input,
			},
			cache:false,
			success:function(quantity){
				$('#cart_input_'+$id_product).attr('value',parseInt(quantity));
				$total = parseInt($price)*parseInt(quantity);
				$('#total_'+$id_product).html($total);
				$('#subtotal').html(parseInt($subtotal) + parseInt($price));
				$('#total_subtotal').html((parseInt($subtotal) + parseInt($price)) - ((parseInt($subtotal) + parseInt($price))*2)/100);
			}
		})
	})
	$('.cart_quantity_down').click(function(){
		$id_product = $(this).attr('id');
		$cart_quantity_input = $('#cart_input_'+$id_product).val();
		$price = $('#price_'+$id_product).val();
		$result_input = parseInt($cart_quantity_input) - 1;
		$subtotal = $('#subtotal').html();
		$total = $('#total_'+$id_product).html();
		if($result_input == 0){
			$('#tr_'+$id_product).remove();
		}
		$.ajax({
			type:"post",
			url :"update/quantity",
            headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			data : {
				id_product : $id_product,
				result_input : $result_input,
			},
			cache:false,
			success:function(quantity){
				$('#cart_input_'+$id_product).attr('value',parseInt(quantity));
				$total = parseInt($price)*parseInt(quantity);
				$('#total_'+$id_product).html($total + "$");
				$('#subtotal').html(parseInt($subtotal) - parseInt($price));
				$('#total_subtotal').html((parseInt($subtotal) - parseInt($price)) - ((parseInt($subtotal) - parseInt($price))*2)/100);
			}
		})
	})
	$('.cart_quantity_delete').click(function(e){
		let id_product = $(this).attr('id');
		let total = $('#total_'+id_product).html();
		$('#tr_'+id_product).remove();
		let subtotal = $('#subtotal').html();
		$.ajax({
			type:"post",
			url : "delete/product",
			headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : {
            	id_product : id_product,
            },
            cache:false,
            success:function(data){
               	$('#subtotal').html(parseInt(subtotal) - parseInt(total));
				$('#total_subtotal').html((parseInt(subtotal) - parseInt(total)) - ((parseInt(subtotal) - parseInt(total))*2)/100);
            	console.log("delete success");
            }
		});
	})
})