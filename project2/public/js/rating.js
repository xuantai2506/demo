$(document).ready(function(){
    // Load trang web sẽ hiện ra số ngôi sao trung bình
    $average_rating = $('.average_rating').attr('value');
    $ceilAverage = Math.ceil($average_rating); // làm tròn average lên
    $('.rate-this').find('i').removeClass('color');
    $('.rate-this').find('.v'+$ceilAverage).addClass('color');
    $('.rate-this').find('.v'+$ceilAverage).prevAll().addClass('color');
    //  kết thúc load trang web

    // hover rating stars
    $('.ratings_stars').hover(
        function() {
            $(this).addClass('ratings_hover');
            $(this).prevAll().addClass('ratings_hover');
            // $(this).nextAll().removeClass('ratings_vote'); 
        },
        function() {
            $(this).removeClass('ratings_hover');
            $(this).prevAll().removeClass('ratings_hover');
            // set_votes($(this).parent());
        }
    );

    // click rating star
	$('.ratings_stars').click(function(){
        $('.ratings_stars').removeClass('ratings_over');
        if ($(this).hasClass('ratings_over')) {
            $('.ratings_stars').removeClass('ratings_over');
            $(this).prevAll().addClass('ratings_over');
        } else {
            $(this).addClass('ratings_over');
            $(this).prevAll().addClass('ratings_over');
        }
        
        $values =  $(this).find("input").val();
        $id_blog = $(".id_blog").attr('id');
        $.ajax({
            type: "post",
            url: "rating" ,
            headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                values :$values,
                id_blog : $id_blog
            }, 
            cache:false ,
            success:function(data){
                $("#rating").html(data);
                $average = $('#notify_all_rating').attr('value');//lấy trung bình cộng ra và in trung bình cộng
                $ceilAverage = Math.ceil($average); // làm tròn average lên để truyền sao

                // Xóa class và add class color tại  v1,v2,v3,v4,v5 
                $('.rate-this').find('i').removeClass('color');
                $('.rate-this').find('.v'+$ceilAverage).addClass('color');
                $('.rate-this').find('.v'+$ceilAverage).prevAll().addClass('color');
                $('.rated').html("This blog is rated "+$average+ " stars"); //gắn vào class rated html


            }
        });

    });
        // $.ajax({
        //     method: "post",
        //     url: "rating/"+$values ,
        //     data: {
        //         value:$values 
        //     }, 
        //     cache:false ,
        //     success:function(data){
        //         $("#notify_rating").html("-> You has rating " +data+" star");
        //     }
        // });


      
        // if ($(this).hasClass('ratings_over')) {
        //     $('.ratings_stars').removeClass('ratings_over');
        //     $(this).prevAll().andSelf().addClass('ratings_over');
        // } else {
        //     $(this).prevAll().andSelf().addClass('ratings_over');
        // }
  

   
})