$(document).ready(function(){
	$("#search").click(function(){
		let category = $("#category").val();
		let brand = $('#brand').val();
		let status = $('#status').val();
		$.ajax({
			type:"patch",
			url :"searchAdvanced",
            headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			data : {
				category : category,
				brand : brand,
				status : status
			},
			cache:false,
			success:function(html){
				$('#container_product_search').html(html);
			}
		})
	})
	
})