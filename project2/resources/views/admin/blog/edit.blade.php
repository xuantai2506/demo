@extends('master.master')
@section('content')
	
     <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">EDIT BLOG</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">add-blog</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
            	<div class="row">
            		<div class="form-group col-md-12">
                        
            			<form enctype="multipart/form-data" class="form-horizontal m-t-30" method="post">
            				@csrf
                            @if(session('success'))
                                <div class="alert alert-success">
                                    <div class="alert-title">{{session('success')}}</div>
                                </div>
                            @endif
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{ csrf_field() }}
            				<div class="form-group">
            					<label>Title Blog</label>
								<input type="text" class="form-control" name="title" value="{{$getEdit['title']}}">
            				</div>
            				<div class="form-group">
            					<label>Author</label>
            					<input type="text" class="form-control" name="author" value="{{$getEdit['author']}}">
            				</div>
            				<div class="form-group">
            					<label>Description</label>
            					<textarea name="description"  class="form-control" id="demo">{{$getEdit['description']}}</textarea>
            				</div>
            				<div class="form-group">
            					<label>Images</label>
            					<input class="form-control" type="file" name="images">
                                <img width="150px" height="150px" src="{{asset('upload/blog/'.$getEdit['images'])}}">
            				</div>
            				<div class="form-group">
            					<button class="btn btn-success">Edit Blog</button>
            				</div>
            			</form>
    				</div>
            	</div>
    			
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>CKEDITOR.replace( 'demo', {
        filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
        filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
    });; 
</script>

@endsection