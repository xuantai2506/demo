@extends('manager.master')
@section('content')
 
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="{{URL::to('account')}}">
											Account
										</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a  href="{{URL::to('account/list')}}">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											My List
										</a>
									</h4>
								</div>
								
							</div>
							<!-- <div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Sportswear
										</a>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">Nike </a></li>
											<li><a href="#">Under Armour </a></li>
											<li><a href="#">Adidas </a></li>
											<li><a href="#">Puma</a></li>
											<li><a href="#">ASICS </a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#mens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Mens
										</a>
									</h4>
								</div>
								<div id="mens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">Fendi</a></li>
											<li><a href="#">Guess</a></li>
											<li><a href="#">Valentino</a></li>
											<li><a href="#">Dior</a></li>
											<li><a href="#">Versace</a></li>
											<li><a href="#">Armani</a></li>
											<li><a href="#">Prada</a></li>
											<li><a href="#">Dolce and Gabbana</a></li>
											<li><a href="#">Chanel</a></li>
											<li><a href="#">Gucci</a></li>
										</ul>
									</div>
								</div>
							</div> -->
						
						</div><!--/category-products-->
					
						<div class="brands_products"><!--brands_products-->
							<h2>Brands</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									<li><a href="#"> <span class="pull-right">(50)</span>Acne</a></li>
								</ul>
							</div>
						</div><!--/brands_products-->
											
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Account</h2>
						<div class="signup-form">
							 @if(session('success'))
                                <div class="alert alert-success">
                                    <div class="alert-title">{{session('success')}}</div>
                                </div>
                            @endif
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{$error}}</li>
									@endforeach
								</ul>
							</div>
							@endif
							<form enctype="multipart/form-data" action="" method="post">
								@csrf
								<input type="hidden" name="_token" value="{!! csrf_token() !!}">
								<input type="text" name="name" placeholder="Name" value="{{$account['name']}}" />
								<input type="email" name="email" placeholder="Email Address" value="{{$account['email']}}" />
								<input type="password" name="password" placeholder="Password" value="" />
								<input type="number" name="phone_number" placeholder="Phone_number" value="{{$account['phone_number']}}">
								<input type="text" name="messages" placeholder="Messages" value="{{$account['messages']}}">
								<select name="id_country">
									@foreach($country as $country)
									 <option     
                                        value="{{$country['id']}}" {{$country->id == $account->id_country ? 'selected' : '' }}>{{$country['name_country']}}
                                     </option>
									{{$country['name_country']}}</option>
									@endforeach
								</select>
								<h3>Avatar</h3><input type="file" name="images">
								<button type="submit" class="btn btn-default">Edit User</button>
							</form>
						</div>
                    </div>
				</div>
			</div>
		</div><!--/recommended_items-->
	</section>

@endsection