@extends('manager.master')
@section('content')
 
 <section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="{{URL::to('account')}}">
											Account
										</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title" data-toggle="collapse">
										<a data-toggle="collapse" href="{{URL::to('account/list')}} ">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											My List
										</a>
									</h4>
								</div>
							</div>
					
						</div><!--/category-products-->
											
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Add - Product</h2>
						<div class="signup-form">
							 @if(session('success'))
                                <div class="alert alert-success">
                                    <div class="alert-title">{{session('success')}}</div>
                                </div>
                            @endif
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{$error}}</li>
									@endforeach
								</ul>
							</div>
							@endif
							<form enctype="multipart/form-data" action="" method="post">
								@csrf
								<input type="hidden" name="_token" value="{!! csrf_token() !!}">
								<input type="text" name="name_product" placeholder="Name" value="" />
								<input type="number" name="price" placeholder="Price">
								
								<select style="height:40px" name="category">
									<option>Please choose category</option>
									@foreach($category as $category)
									<option value="{{$category['id']}}">{{$category['category_name']}}</option>
									@endforeach
								</select>

								<select  style="height:40px ;margin-top: 10px" name="brand">
									<option>Please choose brand</option>
									@foreach($brand as $brand)
									<option value="{{$brand['id']}}">{{$brand['brand_name']}}</option>
									@endforeach
								</select>
								<select style="height:40px ;margin-top: 10px" name="status">
									<option>Please choose status</option>
									<option value="0">New</option>
									<option value="1">Sale</option>
								</select>
								<div style="margin-top: 20px;">
									<input style="width: 40px;float: left;" type="number" name="percent" >
									<span style="float: left;margin-top: 10px">%</span>
								</div>
									
								
								
								<input style="height:40px ;margin-top: 10px" type="text" name="profile" placeholder="Company profile">
								<input type="file" multiple name="images[]">
								<input type="text" style="height: 150px" name="detail" placeholder="Detail">
								
								<button style="float: right;" type="submit" class="btn btn-default">Add Product</button>
							</form>
						</div>
                    </div>
				</div>
			</div>
		</div><!--/recommended_items-->
	</section>

@endsection