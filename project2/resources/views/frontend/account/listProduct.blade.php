@extends('manager.master')
@section('content')
 
 <section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="{{URL::to('account')}}">
											Account
										</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title" data-toggle="collapse">
										<a data-toggle="collapse" href="{{URL::to('account/list')}} ">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											My List
										</a>
									</h4>
								</div>
							</div>
						
						</div><!--/category-products-->
											
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">List -Product</h2>
						@if(session('success'))
                                <div class="alert alert-success">
                                    <div class="alert-title">{{session('success')}}</div>
                                </div>
                        @endif
						<table class="table table-bordered cart_summary">
							<thead style="background: #FE9A2E">
								<tr>
									<th class="id">Id</th>
									<th class="name">Name</th>
									<th class="images">Images</th>
									<th class="price">Price</th>
									<th class="action">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($listProduct as $list)
								<tr>
									<td>{{$list['id']}}</td>
									<td>{{$list['name_product']}}</td>
									<td>
									<?php 
										$images = json_decode($list['images']);
										if($images==[]){
											echo "Have not images" ;
										}else {
											echo "<img style='margin-left:5px;width:50px ;height :50px' src='../upload/product/$images[0]'>";
										}
									?>
									</td>
									<td>{{$list['price']}}</td>
									<td>
										<a href="edit/{{$list['id']}}">Edit</a> /
										<a href="list/{{$list['id']}}">Delete</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="button">
							<button style="float: right;" class="btn btn-primary">
								<a style="color: white" href="{{URL::to('account/add')}}">Add New</a>
							</button>
						</div>
                    </div>
				</div>
			</div>
		</div><!--/recommended_items-->
	</section>

@endsection