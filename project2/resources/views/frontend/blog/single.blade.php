@extends('manager.master')
@section('content')
	<section>
		<!-- lấy id bloog -->
		<input type="hidden" class="id_blog" id="{{$id}}"> 
		<input type="hidden" class="average_rating" value = "{{$average}}">
		<!--  -->
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Sportswear
										</a>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Nike </a></li>
											<li><a href="">Under Armour </a></li>
											<li><a href="">Adidas </a></li>
											<li><a href="">Puma</a></li>
											<li><a href="">ASICS </a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#mens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Mens
										</a>
									</h4>
								</div>
								<div id="mens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Fendi</a></li>
											<li><a href="">Guess</a></li>
											<li><a href="">Valentino</a></li>
											<li><a href="">Dior</a></li>
											<li><a href="">Versace</a></li>
											<li><a href="">Armani</a></li>
											<li><a href="">Prada</a></li>
											<li><a href="">Dolce and Gabbana</a></li>
											<li><a href="">Chanel</a></li>
											<li><a href="">Gucci</a></li>
										</ul>
									</div>
								</div>
							</div>
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#womens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Womens
										</a>
									</h4>
								</div>
								<div id="womens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Fendi</a></li>
											<li><a href="">Guess</a></li>
											<li><a href="">Valentino</a></li>
											<li><a href="">Dior</a></li>
											<li><a href="">Versace</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Kids</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Fashion</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Households</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Interiors</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Clothing</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Bags</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Shoes</a></h4>
								</div>
							</div>
						</div><!--/category-products-->
					
						<div class="brands_products"><!--brands_products-->
							<h2>Brands</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									<li><a href=""> <span class="pull-right">(50)</span>Acne</a></li>
									<li><a href=""> <span class="pull-right">(56)</span>Grüne Erde</a></li>
									<li><a href=""> <span class="pull-right">(27)</span>Albiro</a></li>
									<li><a href=""> <span class="pull-right">(32)</span>Ronhill</a></li>
									<li><a href=""> <span class="pull-right">(5)</span>Oddmolly</a></li>
									<li><a href=""> <span class="pull-right">(9)</span>Boudestijn</a></li>
									<li><a href=""> <span class="pull-right">(4)</span>Rösch creative culture</a></li>
								</ul>
							</div>
						</div><!--/brands_products-->
						
						<div class="price-range"><!--price-range-->
							<h2>Price Range</h2>
							<div class="well">
								 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
								 <b>$ 0</b> <b class="pull-right">$ 600</b>
							</div>
						</div><!--/price-range-->
						
						<div class="shipping text-center"><!--shipping-->
							<img src="{{ asset('images/home/shipping.jpg')}}" alt="" />
						</div><!--/shipping-->
					</div>
				</div>

				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>{{$singleBlog['title']}}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i> {{$singleBlog['author']}}</li>
									<li><i class="fa fa-clock-o"></i> {{$singleBlog['updated_at']->format('h:i')}} pm</li>
									<li><i class="fa fa-calendar"></i> {{$singleBlog['updated_at']->format('y-m-d')}}</li>
								</ul>
								<div class="rate">
									<div class="vote">
										@for($i = 1; $i <= 5; $i++)
											<div class="star_{{$i}} ratings_stars"><input value="{{$i}}" type="hidden"></div>
									    @endfor
									    <span id='rating'></span>
									</div>

		                        </div>
		                        
							</div>
							
							<a href="">
								<img src="{{ asset('upload/blog/'.$singleBlog['images'])}}" alt="">
							</a>
							<div>
								{{$singleBlog['description']}}
											
							</div>
							<div class="pager-area">
								<ul class="pager pull-right">
									<li><a href="{{ URL::to('blog/'.$previous)}}">Pre</a></li>
									<li><a href="{{ URL::to('blog/'.$next)}}">Next</a></li>
								</ul>
							</div>
						</div>
					</div><!--/blog-post-area-->

					<div class="rating-area">
						<ul class="ratings">
							<li >Rate this item:</li>
							<li class="rate-this">
								<i class="fa fa-star v1" ><input type="hidden" value="1"></i>
								<i class="fa fa-star v2" ><input type="hidden" value="2"></i>
								<i class="fa fa-star v3 " ><input type="hidden" value="3"></i>
								<i class="fa fa-star v4" ><input type="hidden" value="4"></i>
								<i class="fa fa-star v5" ><input type="hidden" value="5"></i>
							</li>
							<li class="rated color">(This blog is rated {{$average}} stars )</li>
						</ul>
						<ul class="tag">
							<li>TAG:</li>
							<li><a class="color" href="">Pink <span>/</span></a></li>
							<li><a class="color" href="">T-Shirt <span>/</span></a></li>
							<li><a class="color" href="">Girls</a></li>
						</ul>
					</div><!--/rating-area-->

					<div class="socials-share">
						<a href=""><img src="{{ asset('images/blog/socials.png')}}" alt=""></a>
					</div><!--/socials-share-->
					<div class="replay-box">
						<div class="row">
							<div class="col-sm-12">
								<div class="text-area">
										@if(session('success'))
				                                <div class="alert alert-success">
				                                    <div class="alert-title">{{session('success')}}</div>
				                                </div>
				                            @endif
										@if ($errors->any())
										<div class="alert alert-danger">
											<ul>
												@foreach ($errors->all() as $error)
													<li>{{$error}}</li>
												@endforeach
											</ul>
										</div>
										@endif
									<form method="post">
										@csrf
										<input type="hidden" name="_token" value="{!! csrf_token() !!}">
										<div class="blank-arrow">
											<label>Comments in here !</label>
										</div>
										<span>*</span>
										<input type="text" name="comment">
										<button class="btn btn-primary">Post Comments</button>
									</form>
									
								</div>
							</div>
						</div>
					</div>
					<!--/Repaly Box-->
					<div class="response-area">
						<h2>3 RESPONSES</h2>
						<ul class="media-list">
						@foreach($listComment as $listComments)

						<div >
							<li class="media">
									<div class="media-body">
										<a class="pull-left" href="#">
											<img class="media-object" style="width: 100px" src="{{ asset('upload/member/'.$listComments['images'])}}" alt="">
										</a>
										<ul class="sinlge-post-meta">
											<li><i class="fa fa-user"></i>{{$listComments['member']}}</li>
											<li><i class="fa fa-clock-o"></i>{{$listComments['updated_at']}} </li>
											<li><i class="fa fa-calendar"></i>{{$listComments['updated_at']}} </li>
										</ul>
										<p>{{$listComments['comment']}}</p>
										<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
									</div>
							</li>
							<!-- Hiện comment reply -->
							@foreach($listReplyComment as $value)
								@if($value['comment_id'] == $listComments['id'])
									<li class="media second-media"> <!--reply -->
										<div  class="media-body">
											<a class="pull-left" href="#">
												<img class="media-object" style="width: 100px ;border-radius: 64px" src="{{ asset('upload/member/'.$value['images'])}}" alt="">
											</a>
											<ul class="sinlge-post-meta">
												<li><i class="fa fa-user"></i>{{$value['name']}}</li>
												<li><i class="fa fa-clock-o"></i>11:2pm</li>
												<li><i class="fa fa-calendar"></i>5/7/2019 </li>
											</ul>
											<p>{{$value['reply']}}</p>
											<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
										</div>
									</li>
								@endif
							@endforeach
							<li class="media second-media"> <!--reply -->
								<form method="post" class="form-inline" action="reply/{{$listComments['id']}}">
									@csrf
									<input type="hidden" name="_token" value="{!! csrf_token() !!}">
									<div class="row">
										<div class="col-sm-1">
											<img style="width: 50px ;height: 50px;border-radius: 64px" src="{{ asset('upload/member/'.Auth::user()->images)}}">
										</div>
										<div class="col-sm-11">	
											<input style="border: 1px solid orange" class="form-control input-lg"  type="text" name="reply">
										</div>
									</div>
									
									<!-- <button style="float: right;margin-top: 4px" class="btn btn-success">Reply</button> -->
									
								</form>
							</li> <!--end reply-->
						</div><br>
						@endforeach 
						</ul>					
					</div><!--/Response-area-->
				
				</div>	
			</div>
		</div>
		
		<script type="text/javascript" src="{{ asset('js/rating.js')}}"></script>
	</section>
@endsection