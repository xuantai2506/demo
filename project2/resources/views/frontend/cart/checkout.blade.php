@extends('manager.master')
@section('content')

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Step1</h2>
			</div>
			<div class="checkout-options">
				<h3>New User</h3>
				<p>Checkout options</p>
				<ul class="nav">
					<li>
						<label><input type="checkbox"> Register Account</label>
					</li>
					<li>
						<label><input type="checkbox"> Guest Checkout</label>
					</li>
					<li>
						<a href=""><i class="fa fa-times"></i>Cancel</a>
					</li>
				</ul>
			</div><!--/checkout-options-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					@if(Auth::check() && Auth::user()->level == 1)
					<div style="position: relative" class="col-sm-12">
						<div class="shopper-info">
							<p>Shopper Information</p>
							<div class="form-one">
							@if($errors->any())
			                   <div class="alert alert-danger">
			                       <ul>
			                           @foreach($errors->all() as $error)
			                                <li>{{$error}}</li>
			                           @endforeach
			                       </ul>
			                   </div>
			            	@endif
							<form method="post" action="checkout">
								@csrf
								<input type="hidden" name="_token" value="{!! csrf_token() !!}">
								<input type="text" value="{{Auth::user()->name}}" placeholder="Display Name">
								<input type="text" value="{{Auth::user()->email}}" name="email" placeholder="Email">
								<input type="text" value="0{{Auth::user()->phone_number}}" placeholder="Password">
								<input type="text" value="{{Auth::user()->messages}}" placeholder="Confirm password">
								<textarea style="position: absolute;top: 20px;right:0 ;width: 500px" name="content_order"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
								<button type="submit" class="btn btn-primary">Continue</button>
							</form>
						</div>
					</div>
				</div>
					@else
					
					<div style="position: relative;" class="col-sm-12 clearfix">
						<div class="bill-to">	
							<p>Bill To</p>
							<div class="form-one">
								@if($errors->any())
				                   <div class="alert alert-danger">
				                       <ul>
				                           @foreach($errors->all() as $error)
				                                <li>{{$error}}</li>
				                           @endforeach
				                       </ul>
				                   </div>
				            	@endif
								<form enctype="multipart/form-data" action="checkout/register" method="post">
									@csrf
									<input type="hidden" name="_token" value="{!! csrf_token() !!}">
									<input type="text" name="name" placeholder="Name" value="" />
									<input type="password" name="password" placeholder="Password"/>
									<input type="password" name="check_pass" placeholder="Check-Pass">
									<input type="number" name="phone_number" placeholder="Phone_number">
									<input type="text" name="messages" placeholder="Messages">
									<input type="email" name="email" placeholder="Email Address"/>
									<h6>Avatar</h6><input type="file" name="images">
									<select name="id_country">
										<option>-----Country-----</option>
										@foreach($getCountry as $getCountry )
											<option value="{{$getCountry['id']}}">{{$getCountry['name_country']}}</option>
										@endforeach
									</select>
									<textarea style="position: absolute;top: 20px;right:0 ;width: 500px" name="content_order"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
									<button type="submit" class="btn btn-primary">Continue</button>
									
								</form>
							</div>
						</div>
					</div>
					@endif
					<!-- <div class="col-sm-4">
						<div class="order-message">
							<p>Shipping Order</p>
							<form method="post" action="checkout/register">
								<input type="text" name="content_order">
								<textarea name="content_order"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
							</form>
							
							<label><input type="checkbox"> Shipping to bill address</label>
						</div>	
					</div>				 -->	
				</div>
			</div>
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@foreach($product as $products)
						<tr>
							<td class="cart_product">
								<a href=""><img width="70px" src="{{URL::to('Upload/product/'.$products['images'])}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$products['name_product']}}</a></h4>
								<p>Web ID: 1089772</p>
							</td>
							<td class="cart_price">
								<p>{{$products['price']}}$</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<label class="cart_total_price"  autocomplete="off" size="2">{{$products['quantity']}}</label>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">{{$products['price'] * $products['quantity']}}$</p>
							</td>
						<!-- 	<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td> -->
						</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->
@endsection