<style type="text/css">
	h1 {
		text-align: center;
	}
	#customers {
	  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	}

	#customers td, #customers th {
	  border: 1px solid #ddd;
	  padding: 8px;
	}

	#customers tr:nth-child(even){background-color: #f2f2f2;}

	#customers tr:hover {background-color: #ddd;}

	#customers th {
	  padding-top: 12px;
	  padding-bottom: 12px;
	  text-align: left;
	  background-color: #4CAF50;
	  color: white;
	}
</style>
<h1 style="color: red">{{$messages}}</h1>
<p style="color: red">Đơn Hàng Của Bạn Được Liệt Kê Phía Dưới</p>
<table  class="table table-condensed" id="customers">
		<tr class="cart_menu">
			<th class="image">Stt</th>
			<th class="description">Name</th>
			<th class="price">Price</th>
			<th class="quantity">Quantity</th>
			<th class="total">Total</th>
			<th></th>
		</tr>
		<?php $stt = 0 ?>
		@foreach($demo as $products)
		<tr>
			<td class="cart_product">
				<p>{{$stt = $stt + 1}}</p>
			</td>
			<td class="cart_description">
				<h4><a href="">{{$products['name_product']}}</a></h4>
				<p>Web ID: 1089772</p>
			</td>
			<td class="cart_price">
				<p>{{$products['price']}}$</p>
			</td>
			<td class="cart_quantity">
				<div class="cart_quantity_button">
					<label class="cart_total_price"  autocomplete="off" size="2">{{$products['quantity']}}</label>
				</div>
			</td>
			<td class="cart_total">
				<p class="cart_total_price">{{$products['price'] * $products['quantity']}}$</p>
			</td>
		<!-- 	<td class="cart_delete">
				<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
			</td> -->
		</tr>
		@endforeach
</table>