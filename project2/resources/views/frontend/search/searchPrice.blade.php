@if(session('success'))
    <div class="alert alert-success">
        <div class="alert-title">{{session('success')}}</div>
    </div>
@endif
@foreach($newProduct as $newProduct)
	<div class="col-sm-4">
		<div class="product-image-wrapper">
			<div class="single-products">
				<div class="productinfo text-center">
					<?php 
						$images = $newProduct['images'];
						$newImages = json_decode($images);
					?>
					<img  height="300px" src="upload/product/{{$newImages[0]}}" alt="" />

					@if($newProduct['status'] == 1)
					<h2>{{$newProduct['price'] - (($newProduct['price'] * $newProduct['percent'])/100)}} $</h2>
					@else 
					<h2>{{$newProduct['price']}} $</h2>
					@endif

					<p>{{$newProduct['name_product']}}</p>
					<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
				</div>
				<div class="product-overlay">
					<div class="overlay-content">

						@if($newProduct['status'] == 1)
						<h2>{{$newProduct['price'] - (($newProduct['price'] * $newProduct['percent'])/100)}} $</h2>
						@else 
						<h2>{{$newProduct['price']}} $</h2>
						@endif

						<p>{{$newProduct['name_product']}}</p>
						<form method="post">
							@csrf
							<input type="hidden" name="_token" value="{!! csrf_token() !!}">
							<input type="hidden" name="id_product" value="{{$newProduct['id']}}" name="">
							<button type="submit" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
						</form>
					</div>
				</div>
				@if($newProduct['status'] == 0)
				<img src="images/home/new.png" class="new" alt="" />
				@else 
				<img src="images/home/sale.png" class="new" alt="" />
				@endif
			</div>
			<div class="choose">
				<ul class="nav nav-pills nav-justified">
					<li><a href="product/{{$newProduct['id']}}"><i class="fa fa-plus-square"></i>Detail Product</a></li>
					<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
				</ul>
			</div>
		</div>
	</div>
@endforeach