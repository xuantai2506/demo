<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
</head>
<body>
	@include('manager.header')

	@yield('content')

	@include('manager.footer')
</body>
</html>