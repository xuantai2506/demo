<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
</head>
<body>
	@include('master.header')

	@yield('content')

	@include('master.footer')
</body>
</html>