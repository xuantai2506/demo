<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// test gui? thu
Route::get('demo/send/email',function(){
    return view('send-email-demo');
});
//end test
Route::group([
    'namespace' => 'Frontend',
],function(){

    Route::get('/','HomeController@index')->name('frontend.index');
    Route::post('/','HomeController@postAddProduct')->name('frontend.index.add-product');
    Route::patch('/','SearchController@SearchPrice')->name('frontend.ajax.index.search');// ajax price

    Route::get('product','HomeController@getProduct')->name('fronend.index.product');
    Route::post('product','HomeController@postAddProduct')->name('frontend.index.product');
    Route::patch('product','SearchController@SearchPrice')->name('frontend.ajax.product.search_price');// ajax price

    Route::get('product/{id}','HomeController@getProductDetail')->name('frontend.index.product.detail');
    Route::post('product/{id}','HomeController@postAddProduct')->name('frontend.product.detail');

    Route::get('cart','CartController@index')->name('fronend.cart.index');
    Route::get('checkout','CartController@getCheckout')->name('frontend.cart.checkout');
    Route::post('update/quantity','CartController@UpdateQuantity')->name('ajax.frontend.cart');
    Route::post('delete/product','CartController@DeleteProduct')->name('ajax.frontend.delete.cart');
    Route::post('checkout/register','CartController@RegisterUser')->name('frontend.cart.checkout.register');
    Route::post('checkout','CartController@LoginCheckout')->name('frontend.cart.checkout');

    Route::get('/member/register','MemberController@GetRegister')->name('account.register');
    Route::get('/member/login','MemberController@GetLogin')->name('account.get.login');
    Route::post('/member/register','MemberController@PostRegister')->name('account.post.register');
    Route::post('/member/login','MemberController@PostLogin')->name('account.post.login');

    Route::get('/blog/list','BlogController@GetListBlog')->name('frontend.blog.list');
    Route::get('/blog/{id}','BlogController@GetSingleBlog')->name('frontend.blog.single');

    Route::post('search','SearchController@postSearch')->name('frontend.get.search');
    Route::put('search','HomeController@postAddProduct')->name('frontend.addProduct.search');

    Route::patch('search/price','SearchController@SearchPrice'); // ajax price

    Route::get('searchAdvanced','SearchController@searchAdvance')->name('frontend.search.advance');
    Route::post('searchAdvanced','HomeController@postAddProduct');
    Route::patch('searchAdvanced','SearchController@postSearchAdvance')->name('frontend.search.advance');
    
    Route::group([
        'middleware' => ['member']
    ],function(){
        Route::get('/member/logout','MemberController@PostLogout')->name('account.post.logout');
        Route::post('/blog/rating','BlogController@PostRating')->name('blog.get.rating');

        Route::post('/blog/{id}','CommentController@PostComment')->name('frontend.blog.comment');
        Route::post('/blog/reply/{id}','CommentController@PostReplyComment')->name('frontend.blog.reply.comment');

        Route::get('account','AccountController@index')->name('account.get.account');
        Route::post('account','AccountController@PostAccount')->name('account.post.edit');

        Route::get('account/list','AccountController@GetListProduct')->name('account.list.product');
        Route::get('account/list/{id}','AccountController@DeleteProduct')->name('account.list.delete');
        Route::get('account/add','AccountController@GetNewProduct')->name('account.new.product');
        Route::post('account/add','AccountController@PostNewProduct')->name('account.new.product');

        Route::get('account/edit/{id}','AccountController@GetEditProduct')->name('account.edit.product');
        Route::post('account/edit/{id}','AccountController@PostEditProduct')->name('accout.edit.product');
    });

});

Auth::routes();

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Auth'
], function () {
    Route::get('/', 'LoginController@showLoginForm'); //ham showlogin la ham login mac dinh cua laravel, e muon biet no o dau thi mo router ẩn cua no len la thay. trong bai 5 co lenh show router ân. roi 
    Route::get('/login', 'LoginController@showLoginForm');
    Route::post('/login', 'LoginController@login');
    Route::get('/logout', 'LoginController@logout');
});

Route::group([
	'prefix' => 'admin',
	'namespace' => 'Admin',
    'middleware' => ['admin']
],function(){
    
	Route::get('home', 'UserController@index')->name('admin.home');

    Route::get('/profile', 'UserController@GetPagesProfile')->name('admin.pages-profile');
    Route::post('/profile','UserController@PostPagesProfile')->name('admin.pages-profile');

    Route::get('/form', 'UserController@GetFormBasic')->name('admin.form-basic');

    Route::get('/countries','CountryController@GetCountries')->name('admin.countries');
    Route::post('countries','CountryController@PostCountries')->name('admin.countries');

    Route::get('/blog/add','BlogController@GetAddBlog')->name('admin.blog.addblog');
    Route::post('blog/add','BlogController@PostAddBlog')->name('admin.blog.addblog');

    Route::get('/blog/list','BlogController@GetListBlog')->name('admin.blog.listblog');

    Route::get('/blog/edit/{id}','BlogController@GetEditBlog')->name('admin.blog.list.edit');
    Route::post('blog/edit/{id}','BlogController@PostEditBlog')->name('admin.blog');

    Route::get('/countries/{id}','CountryController@PostDelCountries')->name('admin.del-countries');
    Route::get('blog/{id}','BlogController@PostDelBlog')->name('admin.del.blog');

    Route::get('category/list','CategoryController@index')->name('admin.category.list');
    Route::post('category/list','CategoryController@AddCategory')->name('admin.category.add');
    
    Route::get('brand/list','BrandController@index')->name('admin.brand.list');
    Route::post('brand/list','BrandController@AddBrand')->name('admin.brand.add');
    
    Route::get('promotion','PromotionController@index')->name('admin.promotion.promotion');
});

